let section1 = document.querySelector(".section-1"),
  section = document.querySelectorAll(".section"),
  section2 = document.querySelector(".section-2"),
  section3 = document.querySelector(".section-3"),
  section4 = document.querySelector(".section-4"),
  section5 = document.querySelector(".section-5"),
  section6 = document.querySelector(".section-6"),
  section7 = document.querySelector(".section-7"),
  section8 = document.querySelector(".section-8"),
  slideShow = document.querySelector(".slide-show"),
  section10 = document.querySelector(".section-10"),
  section11 = document.querySelector(".section-11"),
  aDiv = document.getElementsByTagName("div"),
  buttons = document.getElementsByTagName("button"),
  bakingTrigger = document.querySelector(".baking-trigger"),
  designTrigger = document.querySelector(".design-trigger"),
  portraitTrigger = document.querySelector(".portrait-trigger"),
  functionsTrigger = document.querySelector(".functions-trigger"),
  landscapeTrigger = document.querySelector(".landscape-trigger"),
  artTrigger = document.querySelector(".art-trigger"),
  productsTrigger = document.querySelector(".products-trigger"),
  aboutTrigger = document.querySelector(".about-trigger"),
  contactTrigger = document.querySelector(".contact-trigger"),
  captionArrowDown = document.querySelectorAll(".caption-arrow-down"),
  noCaptionArrow = document.querySelectorAll(".no-caption-arrow i"),
  noCaptionImage = document.querySelectorAll(".slide .major"),
  contactArrow = document.querySelector(".contact-arrow i"),
  captions = document.querySelectorAll(".captions"),
  captionImage = document.querySelectorAll(".captions .major"),
  slide = document.querySelectorAll(".slide"),
  slides = document.querySelectorAll(".slides"),
  mePic = document.querySelector(".me-pic"),
  leftArea = document.querySelectorAll(".left-area"),
  centerArea = document.querySelectorAll(".center-area"),
  rightArea = document.querySelectorAll(".right-area"),
  backgroundGradient = document.querySelector(".background-gradient"),
  sections = Array.from(section),
  captionArrowsDown = Array.from(captionArrowDown),
  noCaptionArrowsDown = Array.from(noCaptionArrow),
  noCaptionImages = Array.from(noCaptionImage),
  captionImages = Array.from(captionImage),
  slideAll = Array.from(slide),
  slidesAll = Array.from(slides),
  button = Array.from(buttons);

//////////////// Indicate Active Link \\\\\\\\\\\\\
button.forEach((o) => {
  o.addEventListener("click", (btnClicked) => {
    if (o.classList.contains("baking-trigger")) {
      bakingTrigger.setAttribute("id", "active");
    } else {
      bakingTrigger.removeAttribute("id");
    }
    if (o.classList.contains("design-trigger")) {
      designTrigger.setAttribute("id", "active");
    } else {
      designTrigger.removeAttribute("id");
    }
    if (o.classList.contains("portrait-trigger")) {
      portraitTrigger.setAttribute("id", "active");
    } else {
      portraitTrigger.removeAttribute("id");
    }
    if (o.classList.contains("functions-trigger")) {
      functionsTrigger.setAttribute("id", "active");
    } else {
      functionsTrigger.removeAttribute("id");
    }
    if (o.classList.contains("landscape-trigger")) {
      landscapeTrigger.setAttribute("id", "active");
    } else {
      landscapeTrigger.removeAttribute("id");
    }
    if (o.classList.contains("art-trigger")) {
      artTrigger.setAttribute("id", "active");
    } else {
      artTrigger.removeAttribute("id");
    }
    if (o.classList.contains("products-trigger")) {
      productsTrigger.setAttribute("id", "active");
    } else {
      productsTrigger.removeAttribute("id");
    }
    if (o.classList.contains("about-trigger")) {
      aboutTrigger.setAttribute("id", "active");
    } else {
      aboutTrigger.removeAttribute("id");
    }
    if (o.classList.contains("contact-trigger")) {
      contactTrigger.setAttribute("id", "active");
    } else {
      contactTrigger.removeAttribute("id");
    }
  });
});

//////////////// Moving Stuff \\\\\\\\\\\\\
////////////// BAKING \\\\\\\\\\\\\\
bakingTrigger.addEventListener("click", () => {
  ///// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  ///// Mobile & Desktop \\\\\\
  section2.setAttribute("id", "move-section-in-left"); //slide 'BAKING' in if out
  section3.removeAttribute("id"); //slide 'PORTRAIT' out if in
  section4.removeAttribute("id");
  section5.removeAttribute("id");
  section6.removeAttribute("id");
  section7.removeAttribute("id");
  section11.removeAttribute("id");
  noCaptionArrow.forEach((a) => {
    a.setAttribute("id", "rotate-arrow-180");
  });
});

////////////// DESIGN \\\\\\\\\\\\\\
designTrigger.addEventListener("click", () => {
  ///// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  ///// Mobile & Desktop \\\\\\
  section2.removeAttribute("id");
  section3.removeAttribute("id"); //slide 'PORTRAIT' out if in
  section4.removeAttribute("id");
  section5.removeAttribute("id");
  section6.removeAttribute("id");
  section7.removeAttribute("id");
  section11.setAttribute("id", "move-section-in-left");
  noCaptionArrow.forEach((a) => {
    a.setAttribute("id", "rotate-arrow-180");
  });
});

////////////// PORTRAIT \\\\\\\\\\\\\\
portraitTrigger.addEventListener("click", () => {
  /////// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  /////// Mobile & Desktop \\\\\\
  section2.removeAttribute("id", "move-section-in-left");
  section3.setAttribute("id", "move-section-in-left");
  section4.removeAttribute("id", "move-section-in-left");
  section5.removeAttribute("id", "move-section-in-left");
  section6.removeAttribute("id", "move-section-in-left");
  section7.removeAttribute("id", "move-section-in-left");
  section11.removeAttribute("id");
  noCaptionArrow.forEach((b) => {
    b.setAttribute("id", "rotate-arrow-180");
  });
});

////////////// Functions \\\\\\\\\\\\\\
functionsTrigger.addEventListener("click", () => {
  /////// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  /////// Mobile & Desktop \\\\\\
  section2.removeAttribute("id");
  section3.removeAttribute("id");
  section4.setAttribute("id", "move-section-in-left");
  section5.removeAttribute("id");
  section6.removeAttribute("id");
  section7.removeAttribute("id");
  section11.removeAttribute("id");
  noCaptionArrow.forEach((b) => {
    b.setAttribute("id", "rotate-arrow-180");
  });
});

//////////// Landscape \\\\\\\\\\\\\\
landscapeTrigger.addEventListener("click", () => {
  /////// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  /////// Mobile & Desktop \\\\\\
  section2.removeAttribute("id");
  section3.removeAttribute("id");
  section4.removeAttribute("id");
  section5.setAttribute("id", "move-section-in-left");
  section6.removeAttribute("id");
  section7.removeAttribute("id");
  section11.removeAttribute("id");
  noCaptionArrow.forEach((b) => {
    b.setAttribute("id", "rotate-arrow-180");
  });
});

//////////// Art \\\\\\\\\\\\\\
artTrigger.addEventListener("click", () => {
  /////// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  /////// Mobile & Desktop \\\\\\
  section2.removeAttribute("id");
  section3.removeAttribute("id");
  section4.removeAttribute("id");
  section5.removeAttribute("id");
  section6.setAttribute("id", "move-section-in-left");
  section7.removeAttribute("id");
  section11.removeAttribute("id");
  noCaptionArrowsDown.forEach((b) => {
    b.setAttribute("id", "rotate-arrow-180");
  });
});
//////////// Products \\\\\\\\\\\\\\
productsTrigger.addEventListener("click", () => {
  /////// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  /////// Mobile & Desktop \\\\\\
  section3.removeAttribute("id");
  section2.removeAttribute("id");
  section4.removeAttribute("id");
  section5.removeAttribute("id");
  section6.removeAttribute("id");
  section7.setAttribute("id", "move-section-in-left");
  section11.removeAttribute("id");
  noCaptionArrow.forEach((b) => {
    b.setAttribute("id", "rotate-arrow-180");
  });
});

////////////// ABOUT \\\\\\\\\\\\\\
aboutTrigger.addEventListener("click", () => {
  ///// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 45em)").matches) {
    mePic.setAttribute("id", "me-pic-move-in"); //Move image in if screen size is more then 45em
    section10.removeAttribute("id", "move-section-in-bottom"); //slide 'CONTACT' out
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-left");
  }

  /////// Mobile & Desktop \\\\\\
  section2.removeAttribute("id", "move-section-in-left"); //slide 'BAKING' in if out
  section3.removeAttribute("id", "move-section-in-left"); //slide 'PORTRAIT' out if in
  section5.removeAttribute("id", "move-section-in-left");
  section6.removeAttribute("id", "move-section-in-left");
  section7.removeAttribute("id", "move-section-in-left");
  section4.removeAttribute("id", "move-section-in-left"); //slide 'FUNCTIONS' out if in
  section8.setAttribute("id", "move-section-in-right");
  section11.removeAttribute("id");
  noCaptionArrow.forEach((e) => {
    e.setAttribute("id", "rotate-arrow-180");
  });
});

////////////// CONTACT \\\\\\\\\\\\\\
contactTrigger.addEventListener("click", () => {
  ///// Desktop Only \\\\\\
  if (window.matchMedia("(min-width: 55em)").matches) {
    section2.removeAttribute("id", "move-section-in-left"); //slide 'BAKING' in if out
    section3.removeAttribute("id", "move-section-in-left"); //slide 'PORTRAIT' out if in
    section4.removeAttribute("id", "move-section-in-left"); //slide 'FUNCTIONS' out if in
    section5.removeAttribute("id", "move-section-in-left");
    section6.removeAttribute("id", "move-section-in-left");
    section7.removeAttribute("id", "move-section-in-left");
    section8.removeAttribute("id", "move-section-in-right"); //slide 'ABOUT' out
    section11.removeAttribute("id");
    slideShow.removeAttribute("id", "move-in");
  }

  /////// Mobile Only \\\\\\
  if (window.matchMedia("(max-width: 55em)").matches) {
    section1.setAttribute("id", "move-menu-out-top");
  }

  ///// Mobile & Desktop \\\\\\
  section10.setAttribute("id", "move-section-in-bottom");
  backgroundGradient.style.top = "-50%";
  contactArrow.setAttribute("id", "rotate-arrow-180");
  contactHeaderComesIn();

  section8.removeAttribute("id");
});

noCaptionArrow.forEach((d) => {
  d.addEventListener("click", () => {
    section1.removeAttribute("id");
    section2.removeAttribute("id");
    section3.removeAttribute("id");
    section4.removeAttribute("id");
    section5.removeAttribute("id");
    section6.removeAttribute("id");
    section8.removeAttribute("id");
    section7.removeAttribute("id");
    section11.removeAttribute("id");
    slideShow.removeAttribute("id", "move-in");
    mePic.removeAttribute("id");
    backgroundGradient.style.top = "0";
    noCaptionArrow.forEach((c) => {
      c.removeAttribute("id", "rotate-arrow-180");
    });
  });
});

contactArrow.addEventListener("click", () => {
  contactArrow.removeAttribute("id");
  section1.removeAttribute("id");
  section10.removeAttribute("id");
});

//////////////// BAKING Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaBaking = document.querySelectorAll("#baking .left-area"),
  leftAreasBaking = Array.from(leftAreaBaking);
leftAreasBaking.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftBaking = f.parentNode.parentNode.previousElementSibling;
      leftBaking.setAttribute("id", "move-in-full-width");
    }
  });
});

//////////////// DESIGN Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaNewSection = document.querySelectorAll("#design .left-area"),
  leftAreasNewSection = Array.from(leftAreaNewSection);
leftAreasNewSection.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftBaking = f.parentNode.parentNode.previousElementSibling;
      leftBaking.setAttribute("id", "move-in-full-width");
    }
  });
});

//////////////// Portrait Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaPortrait = document.querySelectorAll("#portrait .left-area"),
  leftAreasPortrait = Array.from(leftAreaPortrait);
leftAreasPortrait.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftPortrait = f.parentNode.parentNode.previousElementSibling;
      leftPortrait.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// FUNCTIONS Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaFunctions = document.querySelectorAll("#functions .left-area"),
  leftAreasFunctions = Array.from(leftAreaFunctions);
leftAreasFunctions.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftFunctions = f.parentNode.parentNode.previousElementSibling;
      leftFunctions.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// LANDSCAPE Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaLandscape = document.querySelectorAll("#landscape .left-area"),
  leftAreasLandscape = Array.from(leftAreaLandscape);
leftAreasLandscape.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftLandscape = f.parentNode.parentNode.previousElementSibling;
      leftLandscape.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// ART Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaArt = document.querySelectorAll("#art .left-area"),
  leftAreasArt = Array.from(leftAreaArt);
leftAreasArt.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftArt = f.parentNode.parentNode.previousElementSibling;
      leftArt.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// PRODUCTS Chevron In Capture Imag To Go Back \\\\\\\\\\\\\
var leftAreaProducts = document.querySelectorAll("#products .left-area"),
  leftAreasProducts = Array.from(leftAreaProducts);
leftAreasProducts.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index !== 0) {
      f.parentNode.parentNode.removeAttribute("id");
      var leftProducts = f.parentNode.parentNode.previousElementSibling;
      leftProducts.setAttribute("id", "move-in-full-width");
    }
  });
});

//////////////// Square In Capture Imag To Go Back To Collection  \\\\\\\\\\\\\
centerArea.forEach((n) => {
  n.addEventListener("click", (l) => {
    /////// Desktop Only \\\\\\
    if (window.matchMedia("(min-width: 55em)").matches) {
      sections.forEach((u) => {
        u.removeAttribute("id", "update-id");
      });
      n.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.setAttribute(
        "id",
        "move-section-in-left"
      );
    }
    n.setAttribute("id", "move-section-in-left");
    slideAll.forEach((l) => {
      l.removeAttribute("id");
    });
    captionImages.forEach((m) => {
      m.removeAttribute("id", "move-in");
    });
    section1.removeAttribute("id", "move-menu-out-left");
  });
});

//////////////// BAKING Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaBaking = document.querySelectorAll("#baking .right-area"),
  rightAreasBaking = Array.from(rightAreaBaking),
  rightAreasBakingLength = rightAreasBaking.length;
rightAreasBaking.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index + 1 !== rightAreasBakingLength) {
      f.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightBaking = f.parentNode.parentNode.nextElementSibling;
      rightBaking.setAttribute("id", "move-in-full-width");
    }
  });
});

//////////////// DESIGN Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaNewSection = document.querySelectorAll("#design .right-area"),
  rightAreasNewSection = Array.from(rightAreaNewSection),
  rightAreasNewSectionLength = rightAreasNewSection.length;
rightAreasNewSection.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index + 1 !== rightAreasNewSectionLength) {
      f.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightBaking = f.parentNode.parentNode.nextElementSibling;
      rightBaking.setAttribute("id", "move-in-full-width");
    }
  });
});

//////////////// PORTRAIT Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaPortrait = document.querySelectorAll("#portrait .right-area"),
  rightAreasPortrait = Array.from(rightAreaPortrait),
  rightAreasPortraitLength = rightAreasPortrait.length;
rightAreasPortrait.forEach((f, index) => {
  f.addEventListener("click", () => {
    if (index + 1 !== rightAreasPortraitLength) {
      f.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightPortrait = f.parentNode.parentNode.nextElementSibling;
      rightPortrait.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// FUNCTIONS Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaFunctions = document.querySelectorAll("#functions .right-area"),
  rightAreasFunctions = Array.from(rightAreaFunctions),
  rightAreasFunctionsLength = rightAreasFunctions.length;
rightAreaFunctions.forEach((h, index) => {
  h.addEventListener("click", () => {
    if (index + 1 !== rightAreasFunctionsLength) {
      h.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightFunctions = h.parentNode.parentNode.nextElementSibling;
      rightFunctions.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// LANDSCAPE Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaLandscape = document.querySelectorAll("#landscape .right-area"),
  rightAreasLandscape = Array.from(rightAreaLandscape),
  rightAreasLandscapeLength = rightAreasLandscape.length;
rightAreaLandscape.forEach((y, index) => {
  y.addEventListener("click", () => {
    if (index + 1 !== rightAreasLandscapeLength) {
      y.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightLandscape = y.parentNode.parentNode.nextElementSibling;
      rightLandscape.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// ART Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaArt = document.querySelectorAll("#art .right-area"),
  rightAreasArt = Array.from(rightAreaArt),
  rightAreasArtLength = rightAreasArt.length;
rightAreaArt.forEach((z, index) => {
  z.addEventListener("click", () => {
    if (index + 1 !== rightAreasArtLength) {
      z.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightArt = z.parentNode.parentNode.nextElementSibling;
      rightArt.setAttribute("id", "move-in-full-width");
    }
  });
});

////////////// Products Chevron In Capture Imag To Go Forward \\\\\\\\\\\\\
var rightAreaProducts = document.querySelectorAll("#products .right-area"),
  rightAreasProducts = Array.from(rightAreaProducts),
  rightAreasProductsLength = rightAreasProducts.length;
rightAreaProducts.forEach((h, index) => {
  h.addEventListener("click", () => {
    if (index + 1 !== rightAreasProductsLength) {
      h.parentNode.parentNode.removeAttribute("id", "move-in-full-width");
      const rightProducts = h.parentNode.parentNode.nextElementSibling;
      rightProducts.setAttribute("id", "move-in-full-width");
    }
  });
});

///////////////// Slide Caption IN \\\\\\\\\\\\\\\\\\\\
noCaptionImages.forEach((h) => {
  h.addEventListener("click", (y) => {
    h.parentNode.parentNode.parentNode.parentNode.id = "update-id";

    slideAll.forEach((f) => {
      f.setAttribute("id", "move-section-out-bottom");
    });
    // slidesAll.forEach((g) => {
    //   g.setAttribute("id", "remove-scroll");
    // });
    captionArrowsDown.forEach((d) => {
      d.setAttribute("id", "rotate-arrow-180");
      d.removeAttribute("rotate-arrow-out");
    });
    captionImages.find((i) => {
      if (i.children[1].src == h.children[0].src) {
        i.setAttribute("id", "move-in-full-width");
      }
    });
    section1.setAttribute("id", "move-menu-out-left");
  });
});

///////////////// Slide Caption OUT \\\\\\\\\\\\\\\\\\\\
captionArrowsDown.forEach((f) => {
  f.addEventListener("click", () => {
    f.parentNode.parentNode.parentNode.parentNode.parentNode.removeAttribute(
      "id"
    );
    f.parentNode.parentNode.parentNode.parentNode.parentNode.setAttribute(
      "id",
      "move-section-in-left"
    );
    slideAll.forEach((l) => {
      l.removeAttribute("id", "move-section-out-bottom");
    });
    captionImages.forEach((j) => {
      j.removeAttribute("id", "move-in-full-width");
    });
    slidesAll.forEach((k) => {
      k.removeAttribute("id", "remove-scroll");
    });
    captionArrowsDown.forEach((g) => {
      g.removeAttribute("id", "rotate-arrow-180");
      g.classList.add("rotate-arrow-out");
    });
  });
});

/////////////////////////// Header Text Animation Contact \\\\\\\\\\\\\\\\\\\\\\\\\
const headerText = document.querySelector(".header-text");
const headerStringText = headerText.textContent;
const headerSplitText = headerStringText.split("");

function contactHeaderComesIn() {
  headerText.textContent = "";
  for (let i = 0; i < headerSplitText.length; i++) {
    headerText.innerHTML += "<span>" + headerSplitText[i] + "</span>";
  }

  let character = 0;
  let contactTimer = setInterval(onTick, 60);

  function onTick() {
    const span = headerText.querySelectorAll("span")[character];
    span.classList.add("animate-span");
    character++;
    if (character === headerSplitText.length) {
      complete();
      return;
    }
  }

  function complete() {
    clearInterval(contactTimer);
    contactTimer = null;
  }
}

/////////////////////////// Footer Text Animation \\\\\\\\\\\\\\\\\\\\\\\\\
const footerText = document.querySelector(".wrapper .footer-text");
const footerStringText = footerText.textContent;
const footerSplitText = footerStringText.split("");

function contactFooterComesIn() {
  footerText.textContent = "";
  for (let i = 0; i < footerSplitText.length; i++) {
    footerText.innerHTML += "<span>" + footerSplitText[i] + "</span>";
  }

  let character = 0;
  let contactTimer = setInterval(onTick, 230);

  function onTick() {
    const span = footerText.querySelectorAll("span")[character];
    span.classList.add("animate-span");
    character++;
    if (character === footerSplitText.length) {
      complete();
      return;
    }
  }

  function complete() {
    clearInterval(contactTimer);
    contactTimer = null;
  }
}

///////////////// Lens Effect on Contact  \\\\\\\\\\\\\\\\\\\\
let imgLens = document.querySelector(".img-lens img");
let lensTimer = null;

function lensTimerFunction() {
  imgLens.setAttribute("id", "img-lens-grow");
}

function setTimer() {
  lensTimer = setInterval(() => {
    lensTimerFunction(1);
  }, 3000);
}
setTimer();

// imgLens.addEventListener("mouseover", () => {
//   imgLens.setAttribute("id", "img-lens-grow-twitch");
//   imgLens.removeAttribute("id", "img-lens-grow-twitch-reverse");
// });

// imgLens.addEventListener("mouseout", () => {
//   imgLens.setAttribute("id", "img-lens-grow-twitch-reverse");
//   imgLens.removeAttribute("id", "img-lens-grow-twitch");
// });
