//////////// Fades to next Image \\\\\\\\\\\\\\\
window.addEventListener(
  "DOMContentLoaded",
  function (e) {
    // Original JavaScript code by Chirp Internet: chirpinternet.eu
    // Please acknowledge use of this code by including this header.

    let stage = document.getElementById("stage");
    let fadeComplete = function (e) {
      stage.appendChild(arr[0]);
    };
    let arr = stage.getElementsByTagName("a");
    for (let i = 0; i < arr.length; i++) {
      arr[i].addEventListener("animationend", fadeComplete, false);
    }
  },
  false
);

//////////// Slides Carousels down after delay \\\\\\\\\\\\\\\
let slider = document.getElementsByClassName("slide-show")[0],
  overview = document.getElementsByClassName("overview")[0],
  logo = document.querySelector(".logo"),
  subLogo = document.querySelector(".logo h2"),
  noCaptionArrowSlider = document.querySelectorAll(".slider-arrow"),
  noCaptionArrows = Array.from(noCaptionArrowSlider);

function slideIn() {
  setTimeout(() => {
    slider.setAttribute("id", "move-in");
    setTimeout(() => {
      noCaptionArrows.forEach((a) => {
        a.setAttribute("id", "rotate-arrow-180");
      });
    }, 1000);
  }, 1000); // seconds after page load
}
slideIn();
if (window.matchMedia("(min-width: 55em)").matches) {
  contactFooterComesIn();
}

logo.addEventListener("click", () => {
  slider.setAttribute("id", "move-in");
  noCaptionArrows.forEach((c) => {
    c.setAttribute("id", "rotate-arrow-180");
  });
});

noCaptionArrows.forEach((b) => {
  b.addEventListener("click", () => {
    slider.removeAttribute("id", "move-in");
    b.removeAttribute("id", "rotate-arrow-180");
    contactFooterComesIn();
  });
});

//////////////// Logo Brings Slide Show Back In \\\\\\\\\\\\\
logo.addEventListener("click", () => {
  slideShow.setAttribute("id", "move-in");
  subLogo.setAttribute("id", "active");
  bakingTrigger.removeAttribute("id");
  portraitTrigger.removeAttribute("id");
  aboutTrigger.removeAttribute("id");
  contactTrigger.removeAttribute("id");
  section2.removeAttribute("id"); //slide 'BAKING' out
  section3.removeAttribute("id"); //slide 'PORTRAIT' out
  section4.removeAttribute("id"); //slide 'FUNCTIONS' out
  section5.removeAttribute("id"); //slide 'LANDSCAPE' out
  section6.removeAttribute("id"); //slide 'ART' out
  section7.removeAttribute("id"); //slide 'PRODUCTS' out
  section8.removeAttribute("id"); //slide 'ABOUT' out
  section10.removeAttribute("id"); //slide 'CONTACT' out
  section11.removeAttribute("id"); //slide 'DESIGN' out
});

//////////// Slides Carousels Down ON Logo Click \\\\\\\\\\\\\\\
// logo.addEventListener("click", () => {
//   slider.setAttribute("id", "move-in");
//   noCaptionArrows.forEach((c) => {
//     c.setAttribute("id", "rotate-arrow-180");
//   });
// });

// noCaptionArrows.forEach((b) => {
//   b.addEventListener("click", () => {
//     slider.removeAttribute("id", "move-in");
//     b.removeAttribute("id", "rotate-arrow-180");
//     console.log("Hello");
//   });
// });
